[![Netlify Status](https://api.netlify.com/api/v1/badges/c6a0b67d-7f94-4676-acd9-03ad7716ee98/deploy-status)](https://app.netlify.com/sites/gridsome-starter-resume/deploys)

# Cameron Baney Resume

Resume for front-end engineer Cameron Baney. Based off of [Gridsome starter resume](https://github.com/LokeCarlsson/gridsome-starter-resume) by [Loke Carlsson](https://github.com/lokecarlsson).
