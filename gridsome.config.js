module.exports = {
  siteName: 'Cameron Baney',
  siteDescription: 'Portfolio for Cameron Baney. Based off of "Gridsome starter resume"',
  siteUrl: 'https://cameronbaney.dev',
  plugins: [{
      use: '@gridsome/plugin-google-analytics',
      options: {
        id: 'UA-9226509-2'
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        cacheTime: 600000
      }
    }
  ],
  css: {
    loaderOptions: {
      scss: {}
    }
  }
}
